var callCounter = 1;

function drawAnimation(){
  var colors = ['#D8F3E8', '#6EAE7F'], circles = [];

  for (var i = 1; i <= 8; i++) {
    var child = document.getElementById('circles-' + i),
      percentage = [90, 81, 50, 55, 50, 50, 50, 30 ];

    circles.push(Circles.create({
      id:         child.id,
      value:		percentage[i-1],
      text:         function(value){return value + '%';},
      radius:     65,
      width:      7,
      duration: 1000,
      colors:     colors
    }));
  }
  ++callCounter;
}


// var w = window.innerWidth
// if (w <= 1000){
//   var callCounter = 1;

//     function drawAnimation(){
//     var colors = ['#D8F3E8', '#6EAE7F'], circles = [];

//     for (var i = 1; i <= 8; i++) {
//       var child = document.getElementById('circles-' + i),
//         percentage = [90, 81, 50, 55, 50, 50, 50, 30 ];

//       circles.push(Circles.create({
//         id:         child.id,
//         value:		percentage[i-1],
//         text:         function(value){return value + '%';},
//         radius:     45,
//         width:      6,
//         duration: 1000,
//         colors:     colors
//       }));
//   }
//   ++callCounter;
// }
// };

var waypoint = new Waypoint({
  element: document.getElementById('professional'),
  handler: function(direction){
    if (callCounter <= 1) {
      return drawAnimation()
    }
  },
  offset: 700
})

$('#first-item').click(function(){
  setTimeout( () => {
    checkedStars.forEach((item) => {
      item.classList.remove('no-opacity');
    })
  }, 300 )

  return drawAnimation()
})

// STARS

var languageTab = $('#languageTab');
var checkedStars = document.querySelectorAll('.checked-star');
var languageTabPan = $('language');
var personalTab = $('#personalTab');

languageTab.click(function(){
  setTimeout(() =>  {
    checkedStars.forEach((item) => {
      item.classList.add('no-opacity');
    })
  }, 300)
});

personalTab.click(function(){
  setTimeout( () => {
    checkedStars.forEach((item) => {
      item.classList.remove('no-opacity');
    })
  }, 300 )
  
})
